Onica.create() Conference [2019]
==============================================

 - `AI/ML <../../../create-aiml-labs>`_
 - `Big Data <../../../create-bigdata-labs>`_
 - `Containers <../../../create-containers-labs/en/create-containers-lab-1/>`_
 - `IoT Cloud <../../../create-iot-hardware-labs/en/lab-01/>`_
 - `IoT Hardware <../../../create-iot-hardware-labs/en/lab-01/>`_
 - `Serverless <../../../create-serverless-labs>`_